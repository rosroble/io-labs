#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include <linux/slab.h>


#define INPUT_MAX_SIZE 1024

static char input_buffer[INPUT_MAX_SIZE];
static unsigned long input_buffer_size = 0;

struct entry {
    unsigned int sum;
    struct entry *next;
};
static struct entry *head_entry;

static dev_t first;
static struct cdev c_dev;
static struct class *cl;

static struct proc_dir_entry *entry;

static int __init proc_example_init(void);

static void __exit proc_example_exit(void);

static int my_open(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: open()\n");
  return 0;
}

static int my_close(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: close()\n");
  return 0;
}

static ssize_t my_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
  struct entry *tmp;
  printk(KERN_INFO "Driver: read()\n");
  tmp = head_entry;
  while (tmp) {
    printk(KERN_INFO "Driver: %u", tmp->sum);
    tmp = tmp->next;
  }
  return 0;
}

static ssize_t my_write(struct file *f, const char __user *buf,  size_t len, loff_t *off)
{
  unsigned int sum;
  unsigned int curr;
  char sym;
  size_t i;
  struct entry *node;
  printk(KERN_INFO "Driver: write()\n");
  input_buffer_size = len;
  if (input_buffer_size > INPUT_MAX_SIZE) {
    input_buffer_size = INPUT_MAX_SIZE;
  }
  if (copy_from_user(input_buffer, buf, input_buffer_size) != 0) {
    printk(KERN_INFO "Driver: copy_from_user failed. Buffer len is %zu. User buf address is %p", len, buf);
    return -EFAULT;
  }
  curr = 0;
  sum = 0;
  printk(KERN_INFO "Driver: before write\n");
  for (i = 0; i < input_buffer_size; i++) {
    sym = input_buffer[i];
    if (sym >= '0' && sym <= '9') {
      curr = 10 * curr + (sym - '0');
    } else {
      sum += curr;
      curr = 0;
    }
  }
  sum += curr;
  node = (struct entry*) kmalloc(sizeof(struct entry), GFP_KERNEL);
  node->sum = sum;
  if (!head_entry) {
    head_entry = node;
  } else {
    struct entry *tmp = head_entry;
    while (tmp->next)
      tmp = tmp->next;
    tmp->next = node;
  }
  return len;
}


static struct file_operations mychdev_fops =
{
  .owner = THIS_MODULE,
  .open = my_open,
  .release = my_close,
  .read = my_read,
  .write = my_write
};

static int my_dev_uevent(struct device *dev, struct kobj_uevent_env *env) {
  add_uevent_var(env, "DEVMODE=%#o", 0666);
  return 0;
}
static int __init ch_drv_init(void)
{
    proc_example_init();
    printk(KERN_INFO "Hello!\n");
    if (alloc_chrdev_region(&first, 0, 1, "ch_dev") < 0)
	  {
		return -1;
	  }
    if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL)
	  {
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    cl->dev_uevent = my_dev_uevent;
    if (device_create(cl, NULL, first, NULL, "var3") == NULL)
	  {
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    cdev_init(&c_dev, &mychdev_fops);
    if (cdev_add(&c_dev, first, 1) == -1)
	  {
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    return 0;
}
 
static void __exit ch_drv_exit(void)
{
    proc_example_exit();
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    printk(KERN_INFO "Bye!!!\n");
}

static ssize_t proc_write(struct file *file, const char __user * ubuf, size_t count, loff_t* ppos) 
{
	printk(KERN_DEBUG "Attempt to write proc file");
	return -1;
}

static ssize_t proc_read(struct file *file, char __user * ubuf, size_t count, loff_t* ppos) 
{	
	char *tmp_buf; 
	size_t ptr;
	struct entry *cur;	
	if (*ppos > 0) return 0;
	ptr = 0;
	cur = head_entry;
	tmp_buf = kmalloc(sizeof(char) * 1024, GFP_KERNEL);
	while (cur) {
		char as_str[64];
	        int len;	
		len = snprintf(as_str, 64, "%d\n", cur->sum);
		strcpy((char*) tmp_buf + ptr, as_str);
		ptr += len;
		cur = cur->next;
	}
	tmp_buf[ptr++] = '\0';
	if (copy_to_user(ubuf, tmp_buf, ptr) != 0)
	{
		printk(KERN_INFO "Driver: proc_read copy_to_user failed");
		return -EFAULT;
	}
	kfree(tmp_buf);
	*ppos += ptr;
	return ptr;
}

static struct proc_ops my_proc_ops = {
    .proc_read = proc_read,
    .proc_write = proc_write
};


static int __init proc_example_init(void)
{
	entry = proc_create("var3", 0444, NULL, &my_proc_ops);
	printk(KERN_INFO "%s: proc file is created\n", THIS_MODULE->name);
	return 0;
}

static void __exit proc_example_exit(void)
{
	proc_remove(entry);
	printk(KERN_INFO "%s: proc file is deleted\n", THIS_MODULE->name);
}
 
module_init(ch_drv_init);
module_exit(ch_drv_exit);
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Author");
MODULE_DESCRIPTION("The first kernel module");

