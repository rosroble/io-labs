# Лабораторная работа 3

Полошков Борис

Харламов Александр

Группа: P33102

Вариант 3

**Название:** "Разработка драйверов сетевых устройств"

**Цель работы:** 

Получить знания и навыки разработки драйверов сетевых интерфейсов для операционной системы Linux.

## Описание функциональности драйвера

Драйвер осуществляет перехват UDP-пакетов, адресуемых на конкретный порт. 
Информация о портах отправителя / получателя перехваченного пакета отправляется в procfs.

## Инструкция по сборке

```bash
sudo apt install build-essential linux-headers-`uname -r`
sudo make
```

## Инструкция пользователя

Установка модуля осуществляется следующим образом:

```bash
sudo insmod lab3.ko
```

В случае успеха в dmesg будут видны информационные сообщения

```bash
[  696.390057] Module udp_catcher3 loaded
[  696.390062] udp_catcher3: create link vni0
[  696.390064] udp_catcher3: registered rx handler for enp0s3
[  696.412589] vni0: device opened
[  696.424537] Another port: 67
[  696.537184] Another port: 68
[  696.537719] Another port: 67
[  696.538252] Another port: 68
[  696.599597] src port: 43300 
               dst port: 53
[  696.599601] Catched packets: 1
[  696.599602] Captured UDP datagram, saddr: 10.0.2.15
[  696.599603] daddr: 10.0.2.3
[  696.599604] Data length: 58. Data:\x9f\xc3\x01
[  696.599605] \x9f\xc3\x01
[  696.600001] src port: 37938 
               dst port: 53
[  696.600004] Catched packets: 2
[  696.600005] Captured UDP datagram, saddr: 10.0.2.15
[  696.600006] daddr: 10.0.2.3
[  696.600007] Data length: 58. Data:\xd2V\x01
[  696.600008] \xd2V\x01
[  696.623408] Another port: 43300
...
```

Проверяем созданный сетевой интерфейс:

```bash
ip a
```

```bash
3: vni0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN group default qlen 1000
    link/ether 08:00:27:8c:e7:9b brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute vni0
       valid_lft 86206sec preferred_lft 86206sec
    inet6 fe80::3370:a18:a14c:3657/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```

Проверяем, что наша директория создалась в procfs:

```bash
ls /proc | grep vni3_v3
```

## Примеры использования

Нужно инициировать передачу на нашу сторону UDP-пакетов. Самый простой для этого способ: открыть, например, медиа-стриминг (YouTube)

После чего проверяем содержимое директории в procfs:
```bash
cat /proc/vni3_v3
```

Можно также посмотреть статистику по перехваченным пакетам:

```bash
ip -s link show vni0
```

```bash
3: vni0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UNKNOWN mode DEFAULT group default qlen 1000
    link/ether 08:00:27:8c:e7:9b brd ff:ff:ff:ff:ff:ff
    RX:  bytes packets errors dropped  missed   mcast           
         30204     103      0       0       0       0 
    TX:  bytes packets errors dropped carrier collsns           
         10938     104      0       0       0       0 
```
