# IO Systems. Lab 2

Работу выполнили:

- Александр Харламов, 33102
- Борис Полошков, 33102

## Цель работы

Получить знания и навыки разработки драйверов блочных устройств для операционной системы Linux.

## Вариант

Один первичный раздел размером 30Мбайт и один расширенный раздел, содержащий два логических раздела размером 10Мбайт каждый.

## Инструкция по использованию

```shell
$ make
$ sudo insmod vram.ko
```

## Работа 


### Разделы устройства

```shell
$ sudo fdisk -l /dev/vramdisk
```

![fdisk](img/fdisk.png)

### Форматирование и монтирование диска

```shell
$ sudo mkfs.vfat -F16 /dev/vramdisk5
$ sudo mkdir -p /mnt/vramdisk5
$ sudo mount /dev/vramdisk5 /mnt/vramdisk5
```

```shell
$ sudo mkfs.vfat -F16 /dev/vramdisk6
$ sudo mkdir -p /mnt/vramdisk6
$ sudo mount /dev/vramdisk6 /mnt/vramdisk6
```

![mkfs](img/mkfs.png)

### Демонтирование

```shell
sudo umount /mnt/vramdisk5
```

## Замеры производительности

### Виртуальный -> виртуальный

```shell
$ sudo dd if=/dev/vramdisk5 of=/dev/vramdisk6 bs=512 count=10240 oflag=direct
```

![virt_to_virt](img/virt_to_virt.png)

### Реальный -> виртуальный

```shell
$ sudo dd if=/dev/sda of=/dev/vramdisk6 bs=512 count=10240 oflag=direct
```

![real_to_virt](img/real_to_virt.png)

## Вывод

В ходе выполнения данной лабораторной работы мы написали писать драйвер блочного устройства, а также познакомились с основными командами по работе с устройством.
